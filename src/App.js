/* Core */
import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";

/* Sections */
import { Filters, Forecast } from './sections';

/* Components */
import { CurrentWeather, Header } from './components';

const queryClient = new QueryClient();

export const App = () => {
    return (
        <QueryClientProvider client={ queryClient }>
            <main>
                <Filters />

                <Header />

                <CurrentWeather />

                <Forecast />
            </main>
        </QueryClientProvider>
    );
};