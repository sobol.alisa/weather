
export interface DayModel {
    currentDay: string;
    name: string;
    type: string;
    selected: boolean;
}
