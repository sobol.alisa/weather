import React from "react";

export const Day: React.FC<DayProps> = props => {
    const isSelected = props.selected ? 'selected': '';

    return (
        <div className={ `day ${ props.type } ${ isSelected }` } >
            <p>{ props.name }</p>
            <span>{ props.temperature }</span>
        </div>
    );
};

/* Types */
interface DayProps {
    temperature: number;
    name: string;
    type: string;
    selected: boolean;
}
