import React from "react";

export const Header = () => {
    return (
        <div className="head">
            <div className="icon cloudy"></div>
            <div className="current-date">
                <p>Пятница</p>
                <span>29 ноября</span>
            </div>
        </div>
    );
};
