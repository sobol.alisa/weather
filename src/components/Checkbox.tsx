import React from "react";

export const Checkbox: React.FC<CheckboxProps> = props => {

    return (
        <span
            className={`checkbox ${ props.selected ? 'selected' : '' } `}
        >
            {props.name}
        </span>
    );
};

/* Types */
interface CheckboxProps {
    name: string,
    selected: boolean,
}
