import React from "react";

export const Input: React.FC<InputProps> = props => {

    return (
        <p className="custom-input">
            <label htmlFor={ props.id }> { props.label }</label>
            <input id={ props.id } type="text" />
        </p>
    );
};

/* Types */
interface InputProps {
    label: string;
    id: string;
}
