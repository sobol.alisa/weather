import React from "react";

/* Components */
import { Checkbox, Input } from '../components';

export const Filters = () => {

    return (
        <div className="filter">
            <Checkbox name="Облачно" selected={false} />
            <Checkbox name="Солнечно" selected />

            <Input label="Минимальная температура" id="min-temperature" />
            <Input label="Максимальная температура" id="max-temperature" />

            <button>Отфильтровать</button>
        </div>
    );
};
