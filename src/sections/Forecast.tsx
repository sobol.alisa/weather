import React from "react";
import { useQuery } from "react-query";
import axios from 'axios';

/* Components */
import { Day } from '../components';

export const Forecast = () => {
    const { isLoading, error, data } = useQuery('data', () =>
        axios.get("https://lab.lectrum.io/rtx/api/v2/forecast?city=Kiev")
    );

    const forecastData = data?.data?.data;

    console.log("data", forecastData)

    const week = forecastData.slice(0, 7).map(data => (
        <Day
            key={ data.id }
            temperature={ data.values.temperature }
            name={ data.values.name }
            type={ data.values.type }
            selected={ false }
        />
    ));

    return (
        <div className="forecast">
            {/*{ week }*/}
        </div>
    );
};
